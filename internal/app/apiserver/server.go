package apiserver

import (
	. "encoding/json"
	"errors"
	"io"
	"log"
	"math/rand"
	"mitsoСhat/internal/app/model"
	"mitsoСhat/internal/app/store"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

type server struct {
	router *mux.Router
	logger *logrus.Logger
	store  store.Store
}

var (
	errorNotAuthorize           = errors.New("no authorization token")
	errIncorrectEmailOrPassword = errors.New("email or password not valid")
	TOKEN_SECRET_RULE           = "mitsoChatSecret"
)

func newServer(store store.Store) *server {
	s := &server{
		router: mux.NewRouter(),
		logger: logrus.New(),
		store:  store,
	}
	s.configureRouter()
	return s
}

func (s *server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}

func (s *server) configureRouter() {
	s.router.HandleFunc("/api/register", s.handleUserCreate()).Methods("POST")
	s.router.HandleFunc("/api/auth", s.handleAuthUser()).Methods("POST")
	s.router.HandleFunc("/api/upload_photo", s.handleUploadPhoto()).Methods("POST")
	s.router.HandleFunc("/api/update_room", s.handleUpdateRoom()).Methods("POST")
	s.router.HandleFunc("/api/update_user", s.handleUpdateUserInfo()).Methods("POST")
	s.router.Handle("/files/avatars/{rest}", http.StripPrefix("/files/avatars/", http.FileServer(http.Dir("files/avatars/"))))
	s.router.Handle("/files/room_images/{rest}", http.StripPrefix("/files/room_images/", http.FileServer(http.Dir("files/room_images/"))))
	s.router.Handle("/files/documents/{rest}", http.StripPrefix("/files/documents/", http.FileServer(http.Dir("files/documents/"))))
	s.router.Handle("/api/get_private_keys",s.handleGetPrivateKeys()).Methods("POST" )
	s.router.Handle("/api/find_user", s.HandleFindUser()).Methods("POST" )
	s.router.Handle("/api/get_room_members", s.HandleGetRoomMembers()).Methods("POST" )
}

func (s *server) HandleGetRoomMembers() http.HandlerFunc {
	type request struct {
		RoomID int `json:"room_id"`
		Token string `json:"token"`
	}

	return  func(w http.ResponseWriter, r *http.Request) {
		req := request{}
		if err := NewDecoder(r.Body).Decode(&req); err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}
		_, err := s.parseToken(req.Token)
		if err != nil {
			s.error(w, r, http.StatusUnauthorized, err)
			return
		}
		err, users := s.store.Room().GetRoomParticipants(req.RoomID)

		if err != nil {
			s.error(w, r, http.StatusBadRequest, err)
		}
		s.respond(w, r, http.StatusOK, users)
	}
}
func (s *server) HandleFindUser() http.HandlerFunc {
	type request struct {
		Query string `json:"query"`
		Token string `json:"token"`
	}

	return  func(w http.ResponseWriter, r *http.Request) {
		req := request{}

		if err := NewDecoder(r.Body).Decode(&req); err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}

		_, err := s.parseToken(req.Token)
		if err != nil {
			s.error(w, r, http.StatusUnauthorized, err)
			return
		}

		err, users := s.store.User().FindByQuery(req.Query)
		if err != nil {
			s.error(w,r,http.StatusInternalServerError, err)
			return
		}

		if &users == nil {
			s.error(w,r,http.StatusInternalServerError, err)
			return
		}


		s.respond(w, r, http.StatusOK, users)

	}
}
func (s *server) handleGetPrivateKeys() http.HandlerFunc {
	type request struct {
		Token string `json:"token"`
	}

	return  func(w http.ResponseWriter, r *http.Request) {
		req := request{}

		if err := NewDecoder(r.Body).Decode(&req); err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}

		email, err := s.parseToken(req.Token)
		if err != nil {
			s.error(w, r, http.StatusUnauthorized, err)
			return
		}
		u, err := s.store.User().FindByEmail(email)

		err, rooms  := s.store.User().GetUserRooms(u.Email, true)

		if err != nil {
			s.error(w, r, http.StatusInternalServerError, err)
		}
		s.respond(w, r, http.StatusOK, &rooms)

	}

}

func (s *server) logRequest(w http.ResponseWriter, r *http.Request) {
	logger := s.logger
	logger.Infof("started %s %s %s", r.Method, r.RequestURI, r.Header)
}


func (s *server) handleUserCreate() http.HandlerFunc {
	type request struct {
		Email    string `json:"email"`
		Name     string `json:"name"`
		Password string `json:"password"`
	}

	type response struct {
		Token   string     `json:"token"`
		Profile model.User `json:"profile"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		req := request{}
		if err := NewDecoder(r.Body).Decode(&req); err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}
		u := &model.User{
			Email:    req.Email,
			Name:     req.Name,
			Password: req.Password,
		}

		 userId, err := s.store.User().Create(u);
		 if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		if err := s.store.Room().AddUserToGlobalRoom(userId); err != nil {
			log.Fatal(err)
		}
		token := s.generateToken(u)

		res := response{
			Token:   token,
			Profile: *u,
		}
		s.respond(w, r, http.StatusCreated, &res)
	}
}

func (s *server) handleGetProfile() http.HandlerFunc {
	type request struct {
		Token string `json:"token"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		s.logRequest(w, r)

		req := request{}
		if err := NewDecoder(r.Body).Decode(&req); err != nil {
			s.error(w, r, http.StatusUnauthorized, err)
			return
		}

		email, err := s.parseToken(req.Token)
		if err != nil {
			s.error(w, r, http.StatusUnauthorized, err)
			return
		}
		u, err := s.store.User().FindByEmail(email)

		if err != nil {
			s.error(w, r, http.StatusNotFound, store.ErrNotValidToken)
			return
		}
		s.respond(w, r, http.StatusOK, &u)
	}
}

func (s *server) parseToken(tokenString string) (string, error) {

	if tokenString == "" {
		return "", store.ErrNotValidToken
	}
	claims := jwt.MapClaims{}
	_, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(TOKEN_SECRET_RULE), nil
	})

	if err != nil {
		return "", store.ErrNotValidToken
	}

	email := claims["email"].(string)

	if email == "" {
		return "", store.ErrNotValidToken
	}
	return email, nil
}

func (s *server) handleAuthUser() http.HandlerFunc {
	type request struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}

	type response struct {
		Token   string     `json:"token"`
		Profile model.User `json:"profile"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		s.logRequest(w, r)
		req := request{}

		if err := NewDecoder(r.Body).Decode(&req); err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}
		u, err := s.store.User().FindByEmail(req.Email)

		if err != nil || !u.ComparePassword(req.Password) {
			s.error(w, r, http.StatusUnauthorized, errIncorrectEmailOrPassword)
			return
		}
		token := s.generateToken(u)

		res := response{
			Token:   token,
			Profile: *u,
		}
		s.respond(w, r, http.StatusOK, &res)
	}
}

func (s *server) handleUpdateRoom() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		type request struct {
			Token    string `json:"token"`
			RoomName string `json:"room_name,omitempty"`
			ImageKey string `json:"image_key,omitempty"`
			RoomID int `json:"room_id"`
		}

		s.logRequest(w,r)

		req := request{}

		if err := NewDecoder(r.Body).Decode(&req); err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}

		email, err := s.parseToken(req.Token)
		if err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}

		_, err = s.store.User().FindByEmail(email)
		if err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}

		if req.RoomName != "" {
			if err := s.store.Room().UpdateRoomName(req.RoomName, req.RoomID); err != nil {
				s.error(w, r, http.StatusInternalServerError, err)
				return
			}
		}

		if req.ImageKey != "" {
			if err := s.store.Room().UpdateAvatar(req.ImageKey, req.RoomID); err != nil {
				s.error(w, r, http.StatusInternalServerError, err)
				return
			}
		}

	}
}

func (s *server) handleUpdateUserInfo() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		type request struct {
			Token    string `json:"token"`
			UserName string `json:"name,omitempty"`
			ImageKey string `json:"image_key,omitempty"`
		}

		s.logRequest(w,r)

		req := request{}

		if err := NewDecoder(r.Body).Decode(&req); err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}

		email, err := s.parseToken(req.Token)
		if err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}

		_, err = s.store.User().FindByEmail(email)
		if err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}

		if req.UserName != "" {
			if err := s.store.User().UpdateUserName(req.UserName,email); err != nil {
				s.error(w, r, http.StatusInternalServerError, err)
				return
			}
		}

		if req.ImageKey != "" {
			if err := s.store.User().UpdateAvatar(req.ImageKey, email); err != nil {
				s.error(w, r, http.StatusInternalServerError, err)
				return
			}
		}

	}
}

func (s *server) handleUploadPhoto() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		type response struct {
			ImageKey string `json:"image_key"`

		}
		s.logRequest(w, r)

		//10 mb limit
		r.Body = http.MaxBytesReader(w, r.Body, 100*1024*1024)
		file, handler, err := r.FormFile("file_name")
		if err != nil {
			s.error(w, r, http.StatusBadRequest, store.ErrBigFile)
			return
		}
		defer file.Close()

		email, err := s.parseToken(r.FormValue("token"))
		if err != nil {
			s.error(w, r, http.StatusUnauthorized, store.ErrNotAuth)
			return
		}
		u, err := s.store.User().FindByEmail(email)

		if err != nil {
			s.error(w, r, http.StatusNotFound, store.ErrNotValidToken)
			return
		}

		isDoc := r.FormValue("is_doc")
		if err != nil {
			s.error(w, r, http.StatusNotFound, store.ErrWrongReq)
		}





		fileName := handler.Filename
		split := strings.Split(fileName, ".")
		size := len(split)
		if size < 2 {
			s.error(w, r, http.StatusInternalServerError, store.InternalError)
			return
		}
		fileExisten := split[size-1]
		defer file.Close()

		roomID := r.FormValue("room_id")

		savedFilePath := ""

		t := time.Now().Unix()


		random := strconv.Itoa(rand.Int())
		if isDoc == "1" {
			savedFilePath = "files/documents/" + strconv.Itoa(u.ID) + "_" + strconv.FormatInt(t, 10) +  random + "." + fileExisten
		} else if roomID == "" {
			savedFilePath = "files/avatars/" + strconv.Itoa(u.ID) + "_" + strconv.FormatInt(t, 10) + random  + "." + fileExisten

		} else {
			savedFilePath = "files/room_images/" + roomID + "_" + strconv.Itoa(u.ID) + "_" + strconv.FormatInt(t, 10) + random + "." + fileExisten
		}

		f, err := os.OpenFile(savedFilePath, os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			s.error(w, r, http.StatusInternalServerError, store.ErrNotCurrectRoomName)
			return
		}
		defer f.Close()
		_, _ = io.Copy(f, file)
		s.respond(w, r, http.StatusOK, &response{
			ImageKey: savedFilePath,
		})
	}
}

func (s *server) error(w http.ResponseWriter, r *http.Request, code int, err error) {
	s.respond(w, r, code, map[string]string{
		"error": err.Error(),
	})
}

func (s *server) respond(w http.ResponseWriter, r *http.Request, code int, data interface{}) {
	w.WriteHeader(code)
	if data != nil {
		_ = NewEncoder(w).Encode(data)
	}
}

func (s *server) fetchToken(w http.ResponseWriter, r *http.Request) (string, error) {
	requestToken := r.Header.Get("Authorization")
	if requestToken == "" {
		return "", errorNotAuthorize
	}
	return requestToken, nil
}

func (s *server) generateToken(u *model.User) string {

	hmacSampleSecret := []byte(TOKEN_SECRET_RULE)

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email": u.Email,
	})

	tokenString, _ := token.SignedString(hmacSampleSecret)
	return tokenString
}
