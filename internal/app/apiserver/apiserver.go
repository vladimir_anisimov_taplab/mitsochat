package apiserver

import (
	"mitsoСhat/internal/app/store"
	"net/http"
)

func Start(config *Config, store store.Store) error {
	srv := newServer(store)
	return  http.ListenAndServe(config.BindAddr, srv)
}
