package sqlstore

import (
	"database/sql"
	_ "github.com/lib/pq"
	"log"
	"mitsoСhat/internal/app/model"
	"mitsoСhat/internal/app/store"
)

type Store struct {
	db                 *sql.DB
	UserRepository     *UserRepository
	RoomRepository     *RoomRepository
	MessageRepository  *MessageRepository
	NewsRepository     *NewsRepository
	PhotoRepository    *PhotoRepository
	DocumentRepository *DocumentRepository
}

func New(db *sql.DB) *Store {

	sqlStore :=  &Store{
		db: db,
	}
	sqlStore.configure()

	return  sqlStore
}

func (s *Store) User() store.UserRepository {
	if s.UserRepository != nil {
		return s.UserRepository
	}
	s.UserRepository = &UserRepository{
		store: s,
	}
	return s.UserRepository
}

func (s *Store) Document() store.DocumentRepository {
	if s.DocumentRepository != nil {
		return s.DocumentRepository
	}
	s.DocumentRepository = &DocumentRepository{
		store: s,
	}
	return s.DocumentRepository
}


func (s *Store) Photo() store.PhotoRepository {
	if s.PhotoRepository != nil {
		return s.PhotoRepository
	}
	s.PhotoRepository = &PhotoRepository{
		store: s,
	}
	return s.PhotoRepository
}

func (s *Store) Room() store.RoomRepository {
	if s.RoomRepository != nil {
		return s.RoomRepository
	}
	s.RoomRepository = &RoomRepository{
		store: s,
	}
	return s.RoomRepository
}

func (s *Store) Message() store.MessageRepository {
	if s.MessageRepository != nil {
		return s.MessageRepository
	}
	s.MessageRepository = &MessageRepository{
		store: s,
	}
	return s.MessageRepository
}

func (s *Store) News() store.NewsRepository {
	if s.NewsRepository != nil {
		return s.NewsRepository
	}
	s.NewsRepository = &NewsRepository{
		store: s,
	}
	return s.NewsRepository
}


func (s *Store) defaultRoom() *model.Room {
	return &model.Room{
		RoomName: "Default room",
		IsGlobal: true,
		PrivateKey: "k3N92Fu1c19GUSoxXXkUjvcsYTK43dGAyzu7CA6Lkiw=",
		IsDirect: false,
	}
}

func (s *Store) configure()  {
	roomRepository := s.Room()
	roomsCount := roomRepository.GetCountOfRooms()
	if roomsCount == 0 {
		if err, _ := roomRepository.CreateRoom(s.defaultRoom()); err != nil {
			log.Fatal(err)
			return
		}
	}
}
