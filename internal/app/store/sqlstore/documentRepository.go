

package sqlstore

import "mitsoСhat/internal/app/model"

type DocumentRepository struct {
	store *Store
}


func (p *DocumentRepository)AddDocument(document *model.Document, messageID int) (error, *model.Document) {
	var photoDoc = document
	println("message id")
	println(messageID)
	println(document.Key)
	println(document.Title)
	println(document.ID)
	err := p.store.db.QueryRow("insert into  documents (key, name, message_id) values ($1, $2, $3) returning id", document.Key, document.Title, messageID).Scan(&photoDoc.ID)
	if err != nil {
		return  err, nil
	}
	return  nil, photoDoc
}

func (p *DocumentRepository)GetDocument(messageID int) (error, []model.Document) {
	rows, err := p.store.db.Query("SELECT  id, key, name, message_id from documents where message_id = $1",messageID)

	if err != nil {
		return err , nil
	}

	var result []model.Document
	for rows.Next() {
		element := model.Document{}
		err := rows.Scan(&element.ID,
			&element.Key,
			&element.Title,
			&element.MessageID,
		)
		if err != nil {
			println(err.Error())
			return err, nil
		}
		result = append(result, element)
	}
	return  nil, result
}
