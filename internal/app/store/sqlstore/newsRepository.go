package sqlstore

import (
	"mitsoСhat/internal/app/model"
	"time"
)

type NewsRepository struct {
	store *Store
}

func (n *NewsRepository)CreateNews(news *model.News ) (error,model.News) {
	news.Date = int(time.Now().Unix())
	var result = news

	err := n.store.db.QueryRow("INSERT INTO news (user_id, text, title, departure_date, uuid, event_date) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id",
		&news.UserId, &news.Text, &news.Title, &news.Date, &news.UUID, &news.EventDate ).Scan(&result.ID)

	return err , *result
}

func (n *NewsRepository)GetAllNews() (error,[]model.News) {
	rows, err := n.store.db.Query("select id, user_id, text, title, departure_date, uuid, event_date from news")

	if err !=nil {
		return err , nil
	}

	var result []model.News
	for rows.Next() {
		element := model.News{}
		err := rows.Scan(
			&element.ID,
			&element.UserId,
			&element.Text,
			&element.Title,
			&element.Date,
			&element.UUID,
			&element.EventDate,
		)
		if err != nil {
			println(err.Error())
			return err, nil
		}
		result = append(result, element)
	}
	return nil, result
}