package sqlstore

import (
	"log"
	"mitsoСhat/internal/app/model"
	"mitsoСhat/internal/app/store"
)


type RoomRepository struct {
	store *Store
}
func (r *RoomRepository) CreateRoom(room *model.Room) (error, *model.Room) {

	if !room.IsValid()  {
		return  store.ErrNotCurrectRoomName, nil
	}
	err := r.store.db.QueryRow("INSERT INTO room (room_name, admin_profile_id, thumb_key, is_global, private_key, is_direct ) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id",
		room.RoomName, room.AdminId, room.ThumbKey, room.IsGlobal, room.PrivateKey, room.IsDirect).Scan(&room.Id)
	if err != nil {
		return  err, nil
	}
	return nil, room
}

func (r *RoomRepository) GetCountOfRooms() int {
	rows, err := r.store.db.Query("SELECT  COUNT(*) as count from room")
	if err != nil {
		log.Fatal(err)
	}
	var count int
	for rows.Next() {
		if err := rows.Scan(&count); err != nil {
			log.Fatal(err)
		}
	}
	return count

}

func (r *RoomRepository) RemoveUserFromRoom(userID, roomID int) error {
	_, err := r.store.db.Query("delete  from participant where user_id = $1 and room_id = $2", userID, roomID)
	if err != nil {
		return err
	}

	return  nil
}


func (r *RoomRepository) GetRoom(id int) (error, *model.Room) {
	room := model.Room{}
	err := r.store.db.QueryRow("SELECT  id, room_name, admin_profile_id, thumb_key, is_global, private_key, is_direct  from room where id = $1",id).Scan(&room.Id, &room.RoomName, &room.AdminId, &room.ThumbKey, &room.IsGlobal, &room.PrivateKey, &room.IsDirect)
	if err != nil {
		return err, nil
	}
	room.PrivateKey = ""
	return err, &room

}

func (r *RoomRepository) UpdateAvatar(imageKey string, roomID int) error {
	var avatarKey string
	err := r.store.db.QueryRow("update room set thumb_key = $1 where id = $2 returning thumb_key", imageKey, roomID).Scan(
		&avatarKey)

	if err != nil && avatarKey != "" {
		return  err
	}
	return  nil
}

func (r *RoomRepository) IsUserMemberOFRoom(userID int, roomID int) bool {

	var count = 0
	err := r.store.db.QueryRow("SELECT count(*)  FROM participant where room_id = $1 and user_id = $2", roomID, userID).Scan(&count)

	if err != nil {
		log.Fatal(err)
	}

	return count == 1
}


func (r *RoomRepository) UpdateRoomName(roomName string, roomID int) error {
	var name string
	err := r.store.db.QueryRow("update room set room_name = $1 where id = $2 returning room_name", roomName, roomID).Scan(
		&roomName)

	if err != nil && name != "" {
		return  err
	}
	return  nil
}

func (r *RoomRepository) GetRoomParticipants(roomId int) (error, []model.User) { // Отъебнет
	rows, err := r.store.db.Query("select user_table.id, user_table.email, user_table.avatar_path, user_table.push_token, user_table.name, user_table.encrypted_password, permission_level  from user_table inner join participant p on user_table.id = p.user_id where room_id = $1",roomId)
	if err != nil {
		return  err, nil
	}

	var users []model.User


	for rows.Next() {
		element := model.User{}
		err := rows.Scan(
			&element.ID,
			&element.Email,
			&element.AvatarPath,
			&element.PushToken,
			&element.Name,
			&element.EncryptedPassword,
			&element.PermissionsLevel)
		if err != nil {
			println(err.Error())
			return err, nil
		}
		users = append(users, element)
	}

	return nil, users
}

//AddUserToRoom(userId int, roomID int) error

func (r *RoomRepository) AddUserToRoom(userId int, roomID int) error {
	_, err := r.store.db.Query("INSERT INTO participant (room_id, user_id) VALUES ($1, $2)", roomID, userId)
	if err != nil {
		return err
	}
	return nil
}


func (r *RoomRepository) AddUserToGlobalRoom(userId int) error{
	_, err := r.store.db.Query("INSERT INTO participant (room_id, user_id) VALUES ($1, $2)", 1, userId)
	if err != nil {
		log.Fatal(err)
	}
	return nil
}