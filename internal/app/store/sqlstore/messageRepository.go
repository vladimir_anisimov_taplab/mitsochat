package sqlstore

import (
	"errors"
	"mitsoСhat/internal/app/model"
	"time"
)

type MessageRepository struct {
	store *Store
}

func (m *MessageRepository) CreateMessage(message *model.Message) (*model.Message, error) {
	message.Date = int(time.Now().Unix())
	err := m.store.db.QueryRow("insert into room_message (message, user_id, uuid, room_id, departure_date) values ($1 , $2, $3, $4, $5) RETURNING id", message.Text, message.UserID, message.UUID,message.RoomId, message.Date).Scan(&message.ID)
	if err != nil {
		println(err.Error())
	}
	return  message, err
}

func (m *MessageRepository)RemoveMessage(messageID int,userId int) error {
	println("remove message ")
	println(messageID)
	rows, err := m.store.db.Query("delete from room_message where id = $1 returning id", messageID)
	if err != nil {
		return err
	}

	if rows.Next() {
		return  nil
	}
	return  errors.New("Wrng req")

}
func (m *MessageRepository) GetMessages(roomID int, limit int) (error, []model.Message) {
	rows, err := m.store.db.Query("select id, message, user_id, uuid, departure_date, room_id from room_message where room_id = $1 order by id desc limit $2", roomID, 100000)

	if err !=nil {
		return err , nil
	}

	var result []model.Message
	for rows.Next() {
		element := model.Message{}
		err := rows.Scan(&element.ID,
			&element.Text,
			&element.UserID,
			&element.UUID,
			&element.Date,
			&element.RoomId)
		if err != nil {
			println(err.Error())
			return err, nil
		}
		result = append(result, element)
	}
	return nil, result
}

