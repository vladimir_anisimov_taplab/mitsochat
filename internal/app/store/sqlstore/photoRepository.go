package sqlstore

import "mitsoСhat/internal/app/model"

type PhotoRepository struct {
	store *Store
}


func (p *PhotoRepository)AddPhoto(photo *model.Photo, messageID int) (error, *model.Photo) {
	var photoReuslt = photo
	err := p.store.db.QueryRow("insert into  photo (photo_key, message_id) values ($1, $2) returning id", photo.Key, messageID).Scan(&photoReuslt.ID)
	if err != nil {
		return  err, nil
	}
	return  nil, photoReuslt
}

func (p *PhotoRepository)GetPhoto(messageID int) (error, []model.Photo) {
	rows, err := p.store.db.Query("SELECT  id, photo_key, message_id  from photo where message_id = $1",messageID)

	if err != nil {
		return err , nil
	}

	var result []model.Photo
	for rows.Next() {
		element := model.Photo{}
		err := rows.Scan(&element.ID,
			&element.Key,
			&element.MessageID,
		)
		if err != nil {
			println(err.Error())
			return err, nil
		}
		result = append(result, element)
	}
	return  nil, result
}

