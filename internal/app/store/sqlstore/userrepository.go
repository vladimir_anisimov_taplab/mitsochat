package sqlstore

import (
	"database/sql"
	"mitsoСhat/internal/app/model"
	"mitsoСhat/internal/app/store"
)

type UserRepository struct {
	store *Store
}

func (r *UserRepository) Create(u *model.User) (int, error) {

	if err := u.Validate(); err != nil {
		return 0, err
	}
	if err := u.BeforeCreate(); err != nil {
		return 0, err
	}

	 err := r.store.db.QueryRow("INSERT INTO user_table (email, name, encrypted_password) VALUES ($1, $2, $3) RETURNING id",
		u.Email,
		u.Name,
		u.EncryptedPassword).Scan(&u.ID)
	return u.ID , err
//
}

func (r *UserRepository) UpdatePushToken(email string, pushToken string) error {
	var token string
	err := r.store.db.QueryRow("update user_table set push_token = $1 where email = $2 returning push_token", pushToken, email).Scan(
		&token)

	if err != nil && token != "" {
		return  err
	}
	return  nil
}
//GetAllUsers() (error, []model.User)

func (r *UserRepository) GetAllUsers() (error, []model.User) {
	rows, err := r.store.db.Query("SELECT id, name, email, encrypted_password, avatar_path, push_token, permission_level from user_table")
	if err != nil {
		return err, nil
	}

	var result []model.User
	for rows.Next() {
		element := model.User{}
		err := rows.Scan(
			&element.ID,
			&element.Name,
			&element.Email,
			&element.EncryptedPassword,
			&element.AvatarPath,
			&element.PushToken,
			&element.PermissionsLevel)
		if err != nil {
			return err, nil
		}
		result = append(result, element)
	}
	return err, result
}

func (r *UserRepository) UpdateAvatar(imageKey string, email string) error {
	var avatar = ""

	err := r.store.db.QueryRow("update user_table set avatar_path = $1 where email = $2 returning avatar_path", imageKey, email).Scan(
		&avatar)

	if err != nil && avatar != "" {
		return  err
	}
	return  nil
}

func (r *UserRepository) UpdateUserName(name string, email string) error {
	var userName = ""

	err := r.store.db.QueryRow("update user_table set name = $1 where email = $2 returning name", name, email).Scan(
		&userName)

	if err != nil && userName != "" {
		return  err
	}
	return  nil
}

func (r *UserRepository) FindByQuery(query string) (error, []model.User)  {
	rows, err := r.store.db.Query("SELECT id, name, email, encrypted_password, avatar_path, push_token, permission_level from user_table where name ~ $1", query)
	if err != nil {
		return err, nil
	}

	var result []model.User
	for rows.Next() {
		element := model.User{}
		err := rows.Scan(
			&element.ID,
			&element.Name,
			&element.Email,
			&element.EncryptedPassword,
			&element.AvatarPath,
			&element.PushToken,
			&element.PermissionsLevel)
		if err != nil {
			return err, nil
		}
		result = append(result, element)
	}
	return err, result
}
//FindByQuery(query string) (error, []model.User)


func (r *UserRepository) FindByEmail(email string) (*model.User, error) {

	u := &model.User{}
	if err := r.store.db.QueryRow("SELECT id, name,  email, encrypted_password, avatar_path, push_token, permission_level from user_table WHERE email = $1", email).Scan(
		&u.ID,
		&u.Name,
		&u.Email,
		&u.EncryptedPassword,
		&u.AvatarPath,
		&u.PushToken,
		&u.PermissionsLevel,


	); err != nil {
		if err == sql.ErrNoRows {
			return nil, store.ErrRecordNotFound
		}
		return nil, err
	}

	return u, nil
}

func (r *UserRepository) FindByID(id int) (*model.User, error) {

	u := &model.User{}
	if err := r.store.db.QueryRow("SELECT id, name, email, encrypted_password, avatar_path, push_token, permission_level from user_table WHERE id = $1", id).Scan(
		&u.ID,
		&u.Name,
		&u.Email,
		&u.EncryptedPassword,
		&u.AvatarPath,
		&u.PushToken,
		&u.PermissionsLevel,
	); err != nil {
		if err == sql.ErrNoRows {
			println(err.Error())
			return nil, store.ErrRecordNotFound
		}
		println(err.Error())
		return nil, err
	}

	return u, nil
}


func (r *UserRepository) GetUserRooms(email string, keys bool) (error, []*model.Room) {
	rows, err := r.store.db.Query("select room.* from room inner join participant p on room.id = p.room_id inner join user_table ut on p.user_id = ut.id where email = $1", email)
	if err != nil {
		return err, nil
	}

	var result []*model.Room
	for rows.Next() {
		element := model.Room{}
		err := rows.Scan(&element.Id, &element.RoomName, &element.AdminId, &element.ThumbKey, &element.IsGlobal, &element.PrivateKey, &element.IsDirect)
		if err != nil {
			return err, nil
		}

		if !keys {
			element.PrivateKey = ""
		}


		result = append(result, &element)
	}
	return nil, result
}