package store

type Store interface {
	User() UserRepository
	Room() RoomRepository
	Message() MessageRepository
	News() NewsRepository
	Photo() PhotoRepository
	Document() DocumentRepository
}
