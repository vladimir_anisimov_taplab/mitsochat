package store

import "mitsoСhat/internal/app/model"

type UserRepository interface {
	Create(user *model.User) (int, error)
	FindByEmail(email string) (*model.User, error)
	GetUserRooms(email string, keys bool) (error, []*model.Room)
	FindByID(id int) (*model.User, error)
	UpdatePushToken(email string, pushToken string) error
	UpdateAvatar(imageKey string, email string) error
	UpdateUserName(name string, email string) error
	GetAllUsers() (error, []model.User)
	FindByQuery(query string) (error, []model.User)
}

type RoomRepository interface {
	CreateRoom(room *model.Room) (error, *model.Room)
	GetCountOfRooms() int
	AddUserToGlobalRoom(userId int) error
	GetRoom(id int) (error, *model.Room)
	GetRoomParticipants(roomId int) (error, []model.User)
	UpdateAvatar(imageKey string, roomID int) error
	UpdateRoomName(roomName string, roomID int) error
	AddUserToRoom(userId int, roomID int) error
	IsUserMemberOFRoom(userID int, roomID int) bool
	RemoveUserFromRoom(userID, roomID int) error
}

type MessageRepository interface {
	CreateMessage(message *model.Message) (*model.Message, error)
	GetMessages(roomID int, limit int) (error, []model.Message)
	RemoveMessage(messageID int, userId int) error
}

type DocumentRepository interface {
	AddDocument(document *model.Document, messageID int) (error, *model.Document)
	GetDocument(messageID int) (error, []model.Document)
}
type NewsRepository interface {
	CreateNews(news *model.News ) (error, model.News)
	GetAllNews() (error,[]model.News)
}

type PhotoRepository interface {
	AddPhoto(photo *model.Photo, messageID int) (error, *model.Photo)
	GetPhoto(messageID int) (error, []model.Photo)
}

