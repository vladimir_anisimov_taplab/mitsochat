package socketserver

import (
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/douglasmakey/go-fcm"
	socketio "github.com/googollee/go-socket.io"
	"log"
	"mitsoСhat/internal/app/apiserver"
	"mitsoСhat/internal/app/model"
	"mitsoСhat/internal/app/store"
	"net/http"
	"strconv"
)

type OnlineUserInfo struct {
	Email     string `json:"email"`
	ID int `json:"id"`
	PushToken string `json:"push_token"`
}

//SocketServer ...
type SocketServer struct {
	Store store.Store
}

var (
	TOKEN_SECRET_RULE = "mitsoChatSecret"
	onlineUsers       map[socketio.Conn]OnlineUserInfo
)

//NewSocketServer init SocketServer
func NewSocketServer(config *apiserver.Config, store store.Store) *SocketServer {
	s := &SocketServer{
		Store: store,
	}
	onlineUsers = make(map[socketio.Conn]OnlineUserInfo)
	  go s.startSocketServer(config)
	return s
}

func (serv *SocketServer) startSocketServer(config *apiserver.Config) error 	{
	server, err := socketio.NewServer(nil)
	if err != nil {
		log.Fatal(err)
	}
	server.OnConnect("/", func(c socketio.Conn) error {
		c.SetContext("")
		fmt.Println("connected:", c.ID())
		return nil
	})


	server.OnEvent("/", "call_request", func(s socketio.Conn, msg string) {
		type request struct {
			UserIDs     []int `json:"user_id"`
			AgoraRoom string `json:"agora_room"`
		}



		req := request{}

		if err := json.Unmarshal([]byte(msg), &req); err != nil {
			println(err.Error())
			sendError(s, store.ErrWrongReq)
			return
		}


		if err != nil {
			sendError(s, store.ErrNotValidToken)
			return
		}


		userInfo, isAuth := onlineUsers[s]

		if isAuth {


			myProfile, err := serv.Store.User().FindByEmail(userInfo.Email)

			if err == nil {

				if err != nil {

					return



				} else {
					for _, userIdentifier  := range req.UserIDs {

						userToCall, error := serv.Store.User().FindByID(userIdentifier)

						if error == nil {
							var user = *userToCall
							var token = *user.PushToken

							tokens := []string{token}

							callPushPush(tokens, myProfile.Name, req.AgoraRoom, myProfile)
						}



					}
				}
			}



			}






	})

	server.OnEvent("/", "on_leave_room", func(s socketio.Conn, msg string) {
		type request struct {
			RoomId int `json:"room_id"`
			IsDirect bool `json:"is_direct"`
		}

		req := request{}
		if err := json.Unmarshal([]byte(msg), &req); err != nil {
			println(err.Error())
			sendError(s, store.ErrWrongReq)
			return
		}

		senderInfo, isAuth := onlineUsers[s]

		if isAuth {

			var err , members = serv.Store.Room().GetRoomParticipants(req.RoomId)

			if err == nil {
				if req.IsDirect {
					for _, member := range members {
						if err := serv.Store.Room().RemoveUserFromRoom(member.ID, req.RoomId); err != nil {
							println(err.Error())
							println("can not remove user from room ")
						} else {
							for connection, user := range onlineUsers {
								if  user.ID == member.ID {
									connection.Emit("on_leave_room", req)
								}
							}
						}


					}
				} else {
					if err := serv.Store.Room().RemoveUserFromRoom(senderInfo.ID, req.RoomId); err != nil {
						println(err.Error())
						println("can not remove user from room ")
					} else {
						for connection, user := range onlineUsers {
							if  user.ID == senderInfo.ID {
								connection.Emit("on_leave_room", req)
							}
						}
					}



				}
			}


		}


	})

	server.OnEvent("/", "add_user_to_chat", func(s socketio.Conn, msg string) {
		type request struct {
			UsersIds     []int `json:"users"`
			RoomID int `json:"room_id"`
		}

		req := request{}
		if err := json.Unmarshal([]byte(msg), &req); err != nil {
			println(err.Error())
			sendError(s, store.ErrWrongReq)
			return
		}


		if err != nil {
			sendError(s, store.ErrNotValidToken)
			return
		}

		log.Print(req.UsersIds)
		for _, userId := range req.UsersIds {

				err := serv.Store.Room().AddUserToRoom(userId, req.RoomID)



			//user, err := serv.Store.User().FindByID(userId)

			if err != nil {
				return
			} else {
				for connection, _ := range onlineUsers {

					//if userInfo.Email == user.Email {

					connection.Emit("events", "rooms_update")

				}

				//}
			}


		}


	})

	server.OnEvent("/", "auth", func(s socketio.Conn, msg string) {

		type request struct {
			Token     string `json:"token"`
			PushToken string `json:"push_token"`
		}


		type roomResponse struct {
			Id       int      `json:"id"`
			RoomName string   `json:"room_name"`
			IsGlobal bool     `json:"is_global"`
			AdminId  *int `json:"admin_id,omitempty"`
			Messages []model.Message `json:"messages,omitempty"`
			ThumbKey *string `json:"thumb_key,omitempty"`
			PrivateKey string `json:"private_key,omitempty"`
			IsDirect bool `json:"is_direct"`
			User model.User `json:"user,omitempty"`
		}

		req := request{}
		if err := json.Unmarshal([]byte(msg), &req); err != nil {
			println(err.Error())
			sendError(s, store.ErrWrongReq)
			return
		}
		email, err := parseToken(req.Token)
		if err != nil {
			sendError(s, store.ErrNotValidToken)
			return
		}

		user, err := serv.Store.User().FindByEmail(email)

		if err != nil {
			sendError(s, store.ErrNotValidToken)
			return
		}
		if err := serv.Store.User().UpdatePushToken(email, req.PushToken); err != nil {
			return
		}

		err, rooms := serv.Store.User().GetUserRooms(email, false)

		if err != nil {
			println(err.Error())
			return
		}
		var response  []roomResponse;
		for i, room := range rooms {
			err, roomMessages := serv.Store.Message().GetMessages(room.Id, 20)

			if err != nil {
				println(err.Error())

				return
			}

			var resultRoomMessages []model.Message
			for _, message := range roomMessages {

				messageOwner, err := serv.Store.User().FindByID(message.UserID)
				if err != nil {
					println(err)

					return
				}

				var photosError, photos  = serv.Store.Photo().GetPhoto(message.ID)

				if photosError == nil {
					message.Photos = photos
				} else {

				}


				var docsError, docs   = serv.Store.Document().GetDocument(message.ID)

				if docsError == nil {
					message.Documents = docs
				} else {

				}


				resultMessage := message
				resultMessage.User = *messageOwner
				resultRoomMessages = append(resultRoomMessages, resultMessage)

			}
			rooms[i].Messages = resultRoomMessages

			roomResponse := roomResponse{
				Id:         rooms[i].Id,
				RoomName:   rooms[i].RoomName,
				IsGlobal:   rooms[i].IsGlobal,
				AdminId:    rooms[i].AdminId,
				Messages:   rooms[i].Messages,
				ThumbKey:   rooms[i].ThumbKey,
				PrivateKey: rooms[i].PrivateKey,
				IsDirect:   rooms[i].IsDirect,

			}

			if rooms[i].IsDirect {
				println("is direct")
				println(roomResponse.Id)
				err, members := serv.Store.Room().GetRoomParticipants(rooms[i].Id)

				if err == nil {
					for _, userMember := range members {
						if userMember.ID != user.ID {
							roomResponse.User = userMember
							println(userMember.Name)
						}
					}
				} else {
					println(err.Error())

				}

			}
			response = append(response, roomResponse)
		}

		onlineUsers[s] = OnlineUserInfo{
			Email:     email,
			PushToken: req.PushToken,
			ID: user.ID,
		}
		println("online users count" +  strconv.Itoa(len(onlineUsers)))

		s.Emit("on_rooms", response)
		err, news := serv.Store.News().GetAllNews()
		if err != nil {
			sendError(s, store.InternalError)
			return
		}

		s.Emit("on_news", news)


	})

	server.OnEvent("/", "on_typing", func(s socketio.Conn, msg string) {

		type request struct {
			RoomID int `json:"room_id"`
			Typing bool `json:"typing"`
		}

		type response struct {

			Typing bool `json:"typing"`
			RoomId int `json:"room_id"`
			Profile *model.User `json:"profile"`
		}
		req := request{}
		if err := json.Unmarshal([]byte(msg), &req); err != nil {
			println(err.Error())
			sendError(s, store.ErrWrongReq)
			return
		}

		senderInfo, isAuth := onlineUsers[s]

		if isAuth {

			user, err := serv.Store.User().FindByEmail(senderInfo.Email)

			if err != nil {
				return
			}

			err, room := serv.Store.Room().GetRoom(req.RoomID)
			if err != nil {
				return
			}

			var response = response{
				Typing: req.Typing,
				RoomId: req.RoomID,
				Profile: user,
			}

				for connection, userInfo := range onlineUsers {
					err, userRooms := serv.Store.User().GetUserRooms(userInfo.Email, false)
					if err != nil {
						continue
					}
					for _, userRoom := range userRooms {
						if userInfo.Email != senderInfo.Email {
							if userRoom.Id == room.Id{
								connection.Emit("on_typing", response)
							}
						}

					}
				}
		} else {
			sendError(s, store.ErrNotAuth)
			return
		}


	})

	server.OnEvent("/", "on_join_chat", func(s socketio.Conn, msg string) {
		type request struct {
			RoomID int    `json:"room_id"`
		}

		req := request{}
		userInfo, isAuth := onlineUsers[s]

		if isAuth {
			if err := json.Unmarshal([]byte(msg), &req); err != nil {
				println(err.Error())
				return
			}

			var isMember = serv.Store.Room().IsUserMemberOFRoom(userInfo.ID, req.RoomID)

			if !isMember {
				if err := serv.Store.Room().AddUserToRoom(userInfo.ID, req.RoomID); err != nil {
					println(err.Error())
					return
				}

				err, room := serv.Store.Room().GetRoom(req.RoomID)

				if err != nil {
					print(err.Error())
					return
				}

				s.Emit("on_join_chat", room)
			}



		} else {
			sendError(s, store.ErrNotAuth)
		}


	})

	server.OnEvent("/", "on_news", func(s socketio.Conn, msg string) {

		_, isAuth := onlineUsers[s]

		if isAuth {
			err, news := serv.Store.News().GetAllNews()
			if err != nil {
				sendError(s, store.InternalError)
				return
			}

			s.Emit("on_news", news)

		} else {
			sendError(s, store.ErrNotAuth)
		}
	})

	server.OnEvent("/", "on_create_news", func(s socketio.Conn, msg string) {
		type request struct {
			Title string `json:"title"`
			Text string `json:"text"`
			UUId string `json:"uuid"`
			EventDate int `json:"event_date,omitempty"`
		}

		req := request{}
		userInfo, isAuth := onlineUsers[s]

		if isAuth {
			if err := json.Unmarshal([]byte(msg), &req); err != nil {
				println(err.Error())
				return
			}

			news := model.News{
				UserId:    userInfo.ID,
				Text:      req.Text,
				Title:     req.Title,
				Date:      req.EventDate,
				UUID:      req.UUId,
			}

			err, news := serv.Store.News().CreateNews(&news)

			if err  != nil {
				sendError(s,store.InternalError)
				return
			}

			s.Emit("on_create_news", news)

			err, users := serv.Store.User().GetAllUsers()

			if err != nil {
				return
			}

			var pushTokens []string
			for _, user := range users {
				token := *user.PushToken
				if user.PushToken != nil && user.PushToken != &userInfo.PushToken{
					pushTokens = append(pushTokens, token)
				}
			}

			sendPush(pushTokens, news.Title, news.Text, news, "news")
		} else {
			sendError(s, store.ErrNotAuth)
			return
		}
	})

	server.OnEvent("/", "on_news", func(s socketio.Conn, msg string) {

		_, isAuth := onlineUsers[s]

		if isAuth {
			err, news := serv.Store.News().GetAllNews()

			if err  != nil {
				sendError(s,store.InternalError)
				return
			}
			s.Emit("on_news", news)
		} else {
			sendError(s, store.ErrNotAuth)
			return
		}
	})

	server.OnEvent("/", "on_chat_message", func(s socketio.Conn, msg string) {

		type docReq struct {
			Key string    `json:"key"`
			FileName   string `json:"file_name"`
		}

		type request struct {
			RoomID int    `json:"room_id"`
			UUID   string `json:"uuid"`
			Photos []string `json:"photos,omitempty"`
			Document []docReq `json:"document,omitempty"`
			Text   string `json:"text"`
		}

		type response struct {
			Id int `json:"id"`
			RoomID int         `json:"room_id"`
			UUID   string      `json:"uuid"`
			Text   string      `json:"text"`
			Sender *model.User `json:"sender"`
			Photos []model.Photo `json:"photos"`
			Doc []model.Document `json:"documents"`
			Date   int         `json:"date"`
		}
		req := request{}
		userInfo, isAuth := onlineUsers[s]

		if isAuth {
			if err := json.Unmarshal([]byte(msg), &req); err != nil {
				println(err.Error())
				return
			}
			message := model.Message{
				RoomId: req.RoomID,
				UUID:   req.UUID,
				Text:   req.Text,
			}

			user, err := serv.Store.User().FindByEmail(userInfo.Email)

			if err != nil {
				println(err.Error())
				return
			}

			if user == nil {
				println("Can not find user in database with email: ", userInfo.Email)
				return
			}
			message.UserID = user.ID
			mes, err := serv.Store.Message().CreateMessage(&message)
			if err != nil {
				println(err.Error())
				return
			}
			log.Print(len(req.Photos))
			log.Print("Photos count")
			var photos  []model.Photo
			for _, el := range  req.Photos {
				var photo = &model.Photo{
					Key:       el,
					MessageID: message.ID,
				}
				err, photo :=serv.Store.Photo().AddPhoto(photo, message.ID)
				if err == nil {
					message.Photos = append(message.Photos, *photo)
					photos = append(photos, *photo)
				} else {
					log.Fatal(err.Error())
				}
			}

			var documents  []model.Document

			for _, el := range  req.Document {
				var doc = &model.Document{

					Key:       el.Key,
					Title:     el.FileName,
					MessageID: 0,
				}
				err, doc := serv.Store.Document().AddDocument(doc, message.ID)

				if err == nil {
					documents = append(documents, *doc)
				} else {
					println(err.Error())
				}


			}

			resp := response{
				Id: mes.ID,
				RoomID: mes.RoomId,
				UUID:   mes.UUID,
				Text:   mes.Text,
				Date:   mes.Date,
				Sender: user,
				Doc: documents,
				Photos: photos,
			}


			err, room := serv.Store.Room().GetRoom(resp.RoomID)

			if err != nil {
				return
			}

			for connection, userInfo := range onlineUsers {
				err, userRooms := serv.Store.User().GetUserRooms(userInfo.Email, false)
				if err != nil {
					continue
				}
				for _, userRoom := range userRooms {
					if userRoom.Id == room.Id {
						connection.Emit("on_chat_message", resp)
					}
				}
			}

			err, roomMemebers := serv.Store.Room().GetRoomParticipants(room.Id)
			//
			if err != nil {
				print(err.Error())
				return
			}

			var pushTokens []string
			for _, memeber := range roomMemebers {

				token := *memeber.PushToken
				if memeber.PushToken != nil && memeber.PushToken != &userInfo.PushToken{
					pushTokens = append(pushTokens, token)
				}

			}

			sendPush(pushTokens, room.RoomName, "✉️ У вас новое сообщение... ", message, "message")
		} else {
			sendError(s, store.ErrNotAuth)
			return
		}

	})

	server.OnEvent("/", "on_rooms", func(s socketio.Conn, msg string) {
		userInfo, isAuth := onlineUsers[s]

		type roomResponse struct {
			Id       int      `json:"id"`
			RoomName string   `json:"room_name"`
			IsGlobal bool     `json:"is_global"`
			AdminId  *int `json:"admin_id,omitempty"`
			Messages []model.Message `json:"messages,omitempty"`
			ThumbKey *string `json:"thumb_key,omitempty"`
			PrivateKey string `json:"private_key,omitempty"`
			IsDirect bool `json:"is_direct"`
			User model.User `json:"user,omitempty"`
		}



		if isAuth {
			err, rooms := serv.Store.User().GetUserRooms(userInfo.Email, false)
			var response  []roomResponse;
			if err != nil {
				println(err.Error())
				return
			}
			for i, room := range rooms {
				err, roomMessages := serv.Store.Message().GetMessages(room.Id, 0)

				if err != nil {
					println(err.Error())
					return
				}

				var reusltRoomMessages []model.Message
				for _, message := range roomMessages {

					messageOwner, errr := serv.Store.User().FindByID(message.UserID)
					if errr != nil {
						println(errr)

						return
					}

					err, photos := serv.Store.Photo().GetPhoto(message.ID)
					if err == nil {
						message.Photos = photos
					}

					errr, docs :=  serv.Store.Document().GetDocument(message.ID)

					if errr == nil {
						message.Documents = docs
					}


					resultMessage := message
					resultMessage.User = *messageOwner
					reusltRoomMessages = append(reusltRoomMessages, resultMessage)

				}
				rooms[i].Messages = reusltRoomMessages

				roomResponse := roomResponse{
					Id:         rooms[i].Id,
					RoomName:   rooms[i].RoomName,
					IsGlobal:   rooms[i].IsGlobal,
					AdminId:    rooms[i].AdminId,
					Messages:   rooms[i].Messages,
					ThumbKey:   rooms[i].ThumbKey,
					PrivateKey: rooms[i].PrivateKey,
					IsDirect:   rooms[i].IsDirect,

				}

				if rooms[i].IsDirect {
					println(roomResponse.Id)
					err, members := serv.Store.Room().GetRoomParticipants(rooms[i].Id)

					if err == nil {
						for _, userMember := range members {
							if userMember.ID != userInfo.ID {
								roomResponse.User = userMember
								println(userMember.Name)

							}


						}
					} else {
						println(err.Error())
						println("not f")
					}

				}
				response = append(response, roomResponse)


			}
			s.Emit("on_rooms", response)

		} else {
			sendError(s, store.ErrNotAuth)
			return
		}

	})

	server.OnEvent("/", "on_delete_message", func(s socketio.Conn, msg string) {
		type request struct {
			Token string `json:"token"`
			MessageID int `json:"message_id"`
			RoomId int `json:"roomId"`
		}

		type response struct {

			MessageID int `json:"message_id"`
			RoomId int `json:"roomId"`
		}


		req := request{}
		if err := json.Unmarshal([]byte(msg), &req); err != nil {
			println(err.Error())
			return
		}
		userInfo, isAuth := onlineUsers[s]

		if isAuth {
			err :=serv.Store.Message().RemoveMessage(req.MessageID, userInfo.ID)
			if err == nil {
				var err, members  = serv.Store.Room().GetRoomParticipants(req.RoomId)

				if err == nil {
					for _, member := range members {
						for connection, memberC := range onlineUsers {
							if member.ID == memberC.ID {
								connection.Emit("on_delete_message", response{
									MessageID: req.MessageID,
									RoomId: req.RoomId,
								})
							}
						}
					}

				}

			} else {
				println(err.Error())
			}

		}

	})

	server.OnEvent("/", "on_create_room", func(s socketio.Conn, msg string) {
		type request struct {
			RoomName      string `json:"room_name"`
			ThumbImageKey string `json:"thumb_image_key,omitempty"`
			PrivateKey string `json:"private_key"`
			IsDirect bool `json:"is_direct"`
			UserId *int `json:"user_id,omitempty"`
		}

		type response struct {
			Id       int      `json:"id"`
			RoomName string   `json:"room_name"`
			IsGlobal bool     `json:"is_global"`
			AdminId  *int `json:"admin_id,omitempty"`
			Messages []model.Message `json:"messages,omitempty"`
			ThumbKey *string `json:"thumb_key,omitempty"`
			PrivateKey string `json:"private_key,omitempty"`
			IsDirect bool `json:"is_direct"`
			User model.User `json:"user"`
		}


		req := request{}

		if err := json.Unmarshal([]byte(msg), &req); err != nil {
			println(err.Error())
			return
		}

		userInfo, isAuth := onlineUsers[s]

		if isAuth {
			creatorProfile, err := serv.Store.User().FindByEmail(userInfo.Email)

			if err != nil {
				println(err.Error())
				return
			}

			reqRoom := model.Room{
				RoomName: req.RoomName,
				IsGlobal: false,
				ThumbKey: &req.ThumbImageKey,
				AdminId:  &creatorProfile.ID,
				PrivateKey: req.PrivateKey,
				IsDirect: req.IsDirect,

			}

			err, room := serv.Store.Room().CreateRoom(&reqRoom)
			if err != nil {
				println(err.Error())
				return
			}

			if err != serv.Store.Room().AddUserToRoom(userInfo.ID, room.Id) {
				print(err.Error())
				return
			}

			res := response{
				Id:         room.Id,
				RoomName:   room.RoomName,
				IsGlobal:   room.IsGlobal,
				AdminId:    room.AdminId,
				Messages:   room.Messages,
				ThumbKey:   room.ThumbKey,
				PrivateKey: room.PrivateKey,
				IsDirect:   room.IsDirect,

			}

			if req.IsDirect && req.UserId != nil  {
				directUser, err := serv.Store.User().FindByID(*req.UserId)
				error := serv.Store.Room().AddUserToRoom(directUser.ID, room.Id)

				if error == nil {
					if err == nil {
						res.User = *directUser
						for connection, user := range onlineUsers {
							if user.ID == directUser.ID {
								connection.Emit("events", "rooms_update")
							}
						}
					}
				}

			}
			s.Emit("on_create_room", res)
		} else {
			sendError(s, store.ErrNotAuth)
			return
		}

	})

	server.OnError("/", func(s socketio.Conn, e error) {
		fmt.Println("Error:", e.Error())

		if e.Error() == "websocket: close 1006 (abnormal closure): unexpected EOF" {
			delete(onlineUsers, s)
			println("disconnect" + s.ID())

		}

	})

	server.OnDisconnect("/", func(s socketio.Conn, reason string) {
		delete(onlineUsers, s)
		fmt.Println("closed", reason)
	})

	go server.Serve()
	defer server.Close()

	http.Handle("/srv/", server)
	log.Println("Socket is starting at" , config.SocketPort)
	log.Fatal(http.ListenAndServe(config.SocketPort, nil))

	return nil
}

func sendError(s socketio.Conn, error error) {
	s.Emit("on_error", error.Error())
}
func parseToken(tokenString string) (string, error) {

	if tokenString == "" {
		return "", store.ErrNotValidToken
	}
	claims := jwt.MapClaims{}
	_, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(TOKEN_SECRET_RULE), nil
	})

	if err != nil {
		return "", store.ErrNotValidToken
	}

	email := claims["email"].(string)

	if email == "" {
		return "", store.ErrNotValidToken
	}
	return email, nil
}

func callPushPush(tokens []string, userName string, agoraRoom string, dataModel interface{}) {

	client := fcm.NewClient("AAAAbgOPVb4:APA91bGTZhVFprUUAGEAOVnzEV_H34OOL_GXHGruuz2supgUUR9pryzDGvj_70OS4iTkJgOU15SdDjL4P7jsXI-8znwlJPEoB2Na_DIhSZI-WqXfaY1BOEnT5xfkNrwxTWid2zC9t3AQ")
	client.Message.Priority = "high"
	notification := &fcm.NotificationPayload{
		Title:            "Входящий звонок",
		Body:             userName,
	}

	data := map[string]interface{}{
		"user": dataModel,
		"agoraRoom": agoraRoom,
		"click_action": "FLUTTER_NOTIFICATION_CLICK",
	}
	client.SetData(data)
	client.PushMultipleNotification(tokens, notification)
	badRegistrations := client.CleanRegistrationIds()
	log.Println(badRegistrations)
	status, err := client.Send()
	if err != nil {
		println(err.Error())
	}
	println(status)

}
func sendPush(tokens []string, roomName string, messageText string, dataModel interface{}, pushType string) {
	log.Print(tokens)
	log.Print(dataModel)
	client := fcm.NewClient("AAAAbgOPVb4:APA91bGTZhVFprUUAGEAOVnzEV_H34OOL_GXHGruuz2supgUUR9pryzDGvj_70OS4iTkJgOU15SdDjL4P7jsXI-8znwlJPEoB2Na_DIhSZI-WqXfaY1BOEnT5xfkNrwxTWid2zC9t3AQ")
	client.Message.Priority = "high"
	notification := &fcm.NotificationPayload{
		Title:            roomName,
		Body:             messageText,
	}

	data := map[string]interface{}{
		pushType: dataModel,
		"click_action": "FLUTTER_NOTIFICATION_CLICK",
	}
	client.SetData(data)
	client.PushMultipleNotification(tokens, notification)
	badRegistrations := client.CleanRegistrationIds()
	log.Println(badRegistrations)
	status, err := client.Send()
	if err != nil {
		println(err.Error())
	}
	println(status)

}
