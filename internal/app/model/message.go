package model

type Message struct {
	ID int `json:"id"`
	UserID int `json:"user_id"`
	RoomId int `json:"room_id"`
	UUID string `json:"uuid"`
	Text string `json:"text"`
	Date int `json:"date"`
	Photos []Photo `json:"photos"`
	Documents []Document `json:"documents"`
	User User `json:"user,omitempty"`
}