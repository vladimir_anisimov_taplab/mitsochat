package model



type Room struct {
	Id       int      `json:"id"`
	RoomName string   `json:"room_name"`
	IsGlobal bool     `json:"is_global"`
	AdminId  *int `json:"admin_id,omitempty"`
	Messages []Message `json:"messages,omitempty"`
	ThumbKey *string `json:"thumb_key,omitempty"`
	PrivateKey string `json:"private_key,omitempty"`
	IsDirect bool `json:"is_direct"`
}


func (r *Room) IsValid() bool {
	return len(r.RoomName) > 0
}