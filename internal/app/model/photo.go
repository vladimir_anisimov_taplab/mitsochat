package model

type Photo struct {
	ID int `json:"id"`
	Key string `json:"key"`
	MessageID int `json:"message_id"`
}
