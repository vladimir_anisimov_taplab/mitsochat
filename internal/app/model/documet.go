package model

type Document struct {
	ID int `json:"id"`
	Key string `json:"key"`
	Title string `json:"file_name"`
	MessageID int `json:"message_id"`
}
