package model

type News struct {
	ID int `json:"id"`
	UserId int `json:"user_id"`
	Text string `json:"text"`
	Title string `json:"title"`
	Date int `json:"date"`
	UUID string `json:"uuid"`
	EventDate *int `json:"event_date"`
	Profile *User `json:"profile"`
}