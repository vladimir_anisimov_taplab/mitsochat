drop table if exists schema_migrations cascade;

drop table if exists room cascade;

drop table if exists participant cascade;

drop table if exists news cascade;

drop table if exists user_table cascade;

drop table if exists photo cascade;

drop table if exists room_message cascade;

drop table if exists documents cascade;

