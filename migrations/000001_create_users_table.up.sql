    create table if not exists schema_migrations
(
    version bigint  not null
        constraint schema_migrations_pkey
            primary key,
    dirty   boolean not null
);

create table if not exists user_table
(
    id                 bigserial not null
        constraint user_table_pkey
            primary key,
    email              varchar   not null,
    avatar_path        varchar,
    permission_level   int default 0,
    push_token         varchar,
    name               varchar   not null,
    encrypted_password varchar   not null
);

create unique index if not exists user_email_uindex
    on user_table (email);

create table if not exists room
(
    id               bigserial   not null
        constraint room_pkey
            primary key,
    room_name        varchar(60) not null,
    admin_profile_id integer,
    thumb_key        text,
    is_global        boolean default false,
    private_key      text not null,
    is_direct       boolean not null
);

create table if not exists participant
(
    room_id bigint    not null,
    user_id bigint    not null
        constraint participant_user_table_id_fk
            references user_table,
    id      bigserial not null
        constraint participant_pk
            primary key
);

create table if not exists news
(
    id serial not null
        constraint news_pk
            primary key,
    user_id integer not null
        constraint news_user_table_id_fk
            references user_table,
    text text not null,
    title text not null,
    departure_date integer not null,
    uuid text not null,
    event_date integer
);


create table if not exists room_message
(
    id             bigserial not null
        constraint room_message_pk
            primary key,
    message        text      not null,
    user_id        integer   not null,
    uuid           text      not null,
    departure_date integer   not null,
    room_id        integer   not null
);

create table if not exists photo
(
    id serial not null
        constraint attachment_pk
            primary key,
    photo_key text not null,
    message_id integer not null
        constraint photo_room_message_id_fk
            references room_message
            ON DELETE CASCADE
);

create unique index if not exists attachment_id_uindex
    on photo (id);


    create table if not exists documents
    (
        id serial not null
            constraint documents_pk
                primary key,
        key text not null,
        name text not null,
        message_id integer not null
            constraint documents_room_message_id_fk
                references room_message
                ON DELETE CASCADE
    );